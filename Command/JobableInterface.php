<?php

namespace TCS\CommandBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface JobableInterface
 * @package TCS\CommandBundle\Command
 */
interface JobableInterface extends LockableInterface
{
    /**
     * Returns the command meaningful title
     * @return string
     */
    public function getTitle();

    /**
     * Returns the command name
     * @return string
     */
    public function getName();

    /**
     * Returns whether the command could be selected in new Job creation
     * @return bool
     */
    public function isExposed();

    /**
     * Returns an array of command names for which the current command cannot be runned at the same time
     * @return string[]
     */
    public function getConflicts();

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function run(InputInterface $input, OutputInterface $output);
}