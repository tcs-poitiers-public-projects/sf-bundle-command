<?php

namespace TCS\CommandBundle\Command;

use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\Lock\Factory;
use Symfony\Component\Lock\Store\FlockStore;

class Locker
{
    /**
     * @var array
     */
    private $locks;

    public function __construct()
    {
        $this->locks = [];
    }

    /**
     * @param LockableInterface $lockable
     * @return bool
     */
    public function acquire(LockableInterface $lockable)
    {
        return $this->doAcquire($lockable->getLockName());
    }

    /**
     * @param string $name
     * @return bool
     */
    private function doAcquire($name)
    {
        // SF 3
        if (class_exists('Symfony\\Component\\Lock\\Lock')) {
            $factory = new Factory(new FlockStore());
            $lock = $factory->createLock($name);

            if ($lock->acquire()) {
                $this->locks[$name] = $lock;

                return true;
            }

            $lock = null;

            return false;

            // SF 2.8
        } elseif (class_exists('Symfony\\Component\\Filesystem\\LockHandler')) {
            $lockHandler = new LockHandler($name);

            if ($lockHandler->lock()) {
                $this->locks[$name] = $lockHandler;

                return true;
            }

            $lockHandler = null;

            return false;

        }

        throw new \RuntimeException('None of the following locking system is supported : Symfony\\Component\\Lock\\Lock or Symfony\\Component\\Filesystem\\LockHandler');
    }

    /**
     * @param LockableInterface $lockable
     * @return bool
     */
    public function release(LockableInterface $lockable)
    {
        return $this->doRelease($lockable->getLockName());
    }

    /**
     * @param string $name
     * @return bool
     */
    private function doRelease($name)
    {
        if (!array_key_exists($name, $this->locks)) {
            return false;
        }

        // the content of locks[$name] can be either an instance of Lock or LockHandler
        // luckily, they both have a release method.
        $this->locks[$name]->release();
        unset($this->locks[$name]);

        return true;
    }

    /**
     * @param LockableInterface $lockable
     * @return bool
     */
    public function isLocked(LockableInterface $lockable)
    {
        if ($this->acquire($lockable)) {
            $this->release($lockable);

            return false;
        }

        return true;
    }
}