<?php

namespace TCS\CommandBundle\Command;

interface RegistryInterface
{
    /**
     * Returns all registered commands
     * @return JobableInterface[]
     */
    public function all();

    /**
     * Returns a registered command by its name
     * @param $name
     * @return JobableInterface
     */
    public function get($name);
}