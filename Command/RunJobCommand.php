<?php

namespace TCS\CommandBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TCS\CommandBundle\Job\Provider\ProviderInterface;
use TCS\CommandBundle\Job\RunnerInterface;

class RunJobCommand extends Command
{
    /**
     * @var RunnerInterface
     */
    private $runner;

    /**
     * @var ProviderInterface
     */
    private $jobProvider;

    public function __construct(RunnerInterface $runner, ProviderInterface $jobProvider)
    {
        $this->runner = $runner;
        $this->jobProvider = $jobProvider;

        parent::__construct();
    }

    /**
     *
     */
    public function configure()
    {
        $this
            ->setName('tcs:job:run')
            ->setDescription('Runs a job')
            ->addArgument('id', InputArgument::REQUIRED, 'Job id to run');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $job = $this->jobProvider->find($input->getArgument('id'));

        if (!$job) {
            $io->error('Invalid job id provided : ' . $input->getArgument('id'));

            return 1;
        }

        try {
            $this->runner->run($job);
        } catch (\Exception $e) {
            $io->error('Exception catched while running job : '.$e->getMessage());

            if ($output->isDebug()) {
                $io->error($e->getTraceAsString());
            }

            return 1;
        }

        return 0;
    }
}