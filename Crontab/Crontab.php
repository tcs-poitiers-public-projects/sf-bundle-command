<?php

namespace TCS\CommandBundle\Crontab;

use TCS\CommandBundle\Crontab\Exception\CrontabRefreshException;
use TCS\CommandBundle\Job\Provider\ProviderInterface;

final class Crontab implements CrontabInterface
{
    /**
     * @var ProviderInterface
     */
    private $provider;

    /**
     * @var DumperInterface
     */
    private $dumper;

    /**
     * @var ProcessInterface
     */
    private $process;

    /**
     * Crontab constructor.
     * @param ProviderInterface $provider
     * @param DumperInterface $dumper
     * @param ProcessInterface $process
     */
    public function __construct(ProviderInterface $provider, DumperInterface $dumper, ProcessInterface $process)
    {
        $this->provider = $provider;
        $this->dumper = $dumper;
        $this->process = $process;
    }

    /**
     * @inheritdoc
     */
    public function refresh()
    {
        try {
            $this->process->save($this->dumpCrontab());
        } catch (\RuntimeException $e) {
            throw new CrontabRefreshException('Unable to refresh crontab', 0, $e);
        }
    }

    /**
     * @inheritdoc
     */
    public function isStale()
    {
        return 0 !== strcmp($this->dumpCrontab(), $this->process->read());
    }

    /**
     * @return string
     */
    private function dumpCrontab()
    {
        return $this->dumper->dump($this->provider->getScheduled());
    }
}