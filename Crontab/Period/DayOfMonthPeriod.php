<?php

namespace TCS\CommandBundle\Crontab\Period;

class DayOfMonthPeriod extends Period
{

    /**
     * @param $value
     * @return bool
     */
    protected function normalizeValue($value)
    {
        return max(1, min(31, (int)$value));
    }

}