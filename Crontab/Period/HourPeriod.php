<?php

namespace TCS\CommandBundle\Crontab\Period;

class HourPeriod extends Period
{

    /**
     * @param $value
     * @return bool
     */
    protected function normalizeValue($value)
    {
        return max(0, min(23, (int)$value));
    }

}