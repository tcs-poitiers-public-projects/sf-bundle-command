<?php

namespace TCS\CommandBundle\Crontab\Period;

class MonthPeriod extends Period
{

    /**
     * @param $value
     * @return bool
     */
    protected function normalizeValue($value)
    {
        return max(1, min(12, (int)$value));
    }

}