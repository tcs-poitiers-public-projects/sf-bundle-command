<?php

namespace TCS\CommandBundle\Crontab;

interface ProcessInterface
{
    /**
     * Save the passed crontab via the system crontab binary
     * @param string $crontab
     * @return void
     */
    public function save($crontab);

    /**
     * Returns the current installed crontab via the system crontab binary
     * @return string
     */
    public function read();
}
