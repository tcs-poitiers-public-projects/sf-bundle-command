<?php

namespace TCS\CommandBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Job
 * @package TCS\CommandBundle\Entity
 * @ORM\Entity(repositoryClass="TCS\CommandBundle\Entity\Repository\JobRepository")
 * @ORM\Table(name="tcs_job")
 */
class Job
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="command_name")
     */
    protected $commandName;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    protected $arguments;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    protected $options;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="manually_triggerable")
     */
    protected $manuallyTriggerable;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * @var Log[]
     * @ORM\OneToMany(targetEntity="TCS\CommandBundle\Entity\Log", mappedBy="job")
     */
    protected $history;

    /**
     * @var Schedule
     * @ORM\OneToOne(targetEntity="TCS\CommandBundle\Entity\Schedule", mappedBy="job")
     */
    protected $schedule;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * Job constructor.
     */
    public function __construct()
    {
        $this->history = new ArrayCollection();

        $this->arguments = [];
        $this->options = [];

        $this->enabled = true;
        $this->manuallyTriggerable = true;

        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCommandName()
    {
        return $this->commandName;
    }

    /**
     * @param $commandName
     * @return $this
     */
    public function setCommandName($commandName)
    {
        $this->commandName = $commandName;

        return $this;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param $arguments
     * @return $this
     */
    public function setArguments($arguments)
    {
        $this->arguments = $arguments;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isManuallyTriggerable()
    {
        return $this->manuallyTriggerable;
    }

    /**
     * @param $manuallyTriggerable
     * @return $this
     */
    public function setManuallyTriggerable($manuallyTriggerable)
    {
        $this->manuallyTriggerable = $manuallyTriggerable;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param $enabled
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param Log $log
     * @return $this
     */
    public function addLog(Log $log)
    {
        if (!$this->history->contains($log)) {
            $this->history->add($log);
            $log->setJob($this);
        }

        return $this;
    }

    /**
     * @param Log $log
     * @return $this
     */
    public function removeLog(Log $log)
    {
        if ($this->history->contains($log)) {
            $this->history->removeElement($log);
            $log->setJob(null);
        }

        return $this;
    }

    /**
     * @return Schedule
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @param Schedule $schedule
     * @return $this
     */
    public function setSchedule(Schedule $schedule = null)
    {
        if ($this->schedule) {
            $this->schedule->setJob(null);
        }

        $this->schedule = $schedule;

        if ($this->schedule) {
            $this->schedule->setJob($this);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isScheduled()
    {
        return null !== $this->schedule;
    }

}
