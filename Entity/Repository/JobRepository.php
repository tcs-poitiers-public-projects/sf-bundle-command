<?php

namespace TCS\CommandBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class JobRepository extends EntityRepository
{

    /**
     * @return array
     */
    public function findScheduled()
    {
        $queryBuilder = $this
            ->createQueryBuilder('j')
            ->innerJoin('j.schedule', 's');

        try {
            return $queryBuilder
                ->getQuery()
                ->getResult();

        } catch (NoResultException $e) {
            return [];
        }
    }
}