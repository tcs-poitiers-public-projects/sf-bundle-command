<?php

namespace TCS\CommandBundle\Job\Context;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TCS\CommandBundle\Entity\Job;
use TCS\CommandBundle\Job\Context\Event\EndEvent;
use TCS\CommandBundle\Job\Context\Event\ProgressEvent;
use TCS\CommandBundle\Job\Context\Event\StartEvent;

class Context
{
    /**
     * @var Job
     */
    private $job;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var string
     */
    private $key;

    public function __construct($key, Job $job, EventDispatcherInterface $eventDispatcher)
    {
        $this->key = $key;
        $this->job = $job;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     *
     */
    public function start()
    {
        $this->eventDispatcher->dispatch(
            Events::START,
            new StartEvent($this)
        );
    }

    /**
     * @param $statusCode
     */
    public function end($statusCode)
    {
        $this->eventDispatcher->dispatch(
            Events::END,
            new EndEvent($this, $statusCode)
        );
    }

    /**
     * WARNING : this method can cause Doctrine flushes to occur !
     * @param float $progress
     */
    public function progress($progress)
    {
        $this->eventDispatcher->dispatch(
            Events::PROGRESS,
            new ProgressEvent($this, $progress)
        );
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }
}