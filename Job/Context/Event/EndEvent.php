<?php

namespace TCS\CommandBundle\Job\Context\Event;

use TCS\CommandBundle\Job\Context\Context;

class EndEvent extends Event
{
    /**
     * @var int
     */
    private $statusCode;

    public function __construct(Context $context, $statusCode, \DateTime $date = null)
    {
        parent::__construct($context, $date);

        $this->statusCode = $statusCode;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}