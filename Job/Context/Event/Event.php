<?php

namespace TCS\CommandBundle\Job\Context\Event;

use TCS\CommandBundle\Job\Context\Context;

abstract class Event extends \Symfony\Component\EventDispatcher\Event
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var \DateTime
     */
    private $date;

    public function __construct(Context $context, \DateTime $date = null)
    {
        $this->context = $context;
        $this->date = $date ?: new \DateTime();
    }

    /**
     * @return Context
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}