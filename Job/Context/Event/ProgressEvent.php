<?php

namespace TCS\CommandBundle\Job\Context\Event;

use TCS\CommandBundle\Job\Context\Context;

class ProgressEvent extends Event
{
    /**
     * @var float
     */
    private $progress;

    public function __construct(Context $context, $progress, \DateTime $date = null)
    {
        parent::__construct($context, $date);

        $this->progress = $progress;
    }

    /**
     * @return float
     */
    public function getProgress()
    {
        return $this->progress;
    }
}