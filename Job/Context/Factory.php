<?php

namespace TCS\CommandBundle\Job\Context;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TCS\CommandBundle\Job\Provider\ProviderInterface;

class Factory
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ProviderInterface
     */
    private $provider;

    public function __construct(EventDispatcherInterface $eventDispatcher, ProviderInterface $provider)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->provider = $provider;
    }

    /**
     * @param $contextKey
     * @return Context
     */
    public function createFromKey($contextKey)
    {
        $encoder = new KeyEncoder();
        list($jobId, $key) = $encoder->decode($contextKey);

        if (!($job = $this->provider->find($jobId))) {
            throw new \RuntimeException('Provided job ID is invalid : ' . $jobId);
        }

        return new Context($key, $job, $this->eventDispatcher);
    }
}