<?php

namespace TCS\CommandBundle\Job\Context\Listener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TCS\CommandBundle\Entity\Job;
use TCS\CommandBundle\Entity\Log;
use TCS\CommandBundle\Job\Context\Event\EndEvent;
use TCS\CommandBundle\Job\Context\Event\ProgressEvent;
use TCS\CommandBundle\Job\Context\Event\StartEvent;
use TCS\CommandBundle\Job\Context\Events;

class LogSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Log[]
     */
    private $logs;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->logs = [];
    }

    /**
     *
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::START    => 'onStart',
            Events::PROGRESS => 'onProgress',
            Events::END      => 'onEnd',
        ];
    }

    /**
     * @return \TCS\CommandBundle\Entity\Repository\LogRepository
     */
    protected function getLogRepository()
    {
        return $this->entityManager->getRepository(Log::class);
    }

    /**
     * @param $key
     * @return Log
     */
    protected function findLog($key)
    {
        return $this->getLogRepository()->find($key);;
    }

    /**
     * @param $key
     * @param Job $job
     * @return Log
     */
    protected function getOrCreateLog($key, Job $job)
    {
        if (!array_key_exists($key, $this->logs)) {
            $log = $this->findLog($key);

            if (!$log) {
                $log = new Log($key);
                $log->setJob($job);

                $this->entityManager->persist($log);
            }

            $this->logs[$key] = $log;
        }

        return $this->logs[$key];
    }

    /**
     * @param StartEvent $event
     */
    public function onStart(StartEvent $event)
    {
        $key = $event->getContext()->getKey();

        $log = $this->getOrCreateLog($key, $event->getContext()->getJob());
        $log->setStartedAt($event->getDate());

        $this->entityManager->flush();
    }

    /**
     * @param ProgressEvent $event
     */
    public function onProgress(ProgressEvent $event)
    {
        $key = $event->getContext()->getKey();

        $log = $this->getOrCreateLog($key, $event->getContext()->getJob());
        $log->setProgress($event->getProgress());

        $this->entityManager->flush();
    }

    /**
     * @param EndEvent $event
     */
    public function onEnd(EndEvent $event)
    {
        $key = $event->getContext()->getKey();

        $log = $this->getOrCreateLog($key, $event->getContext()->getJob());

        $log->setEndedAt($event->getDate());
        $log->setExitCode($event->getStatusCode());

        $this->entityManager->flush();
    }
}