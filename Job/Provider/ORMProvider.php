<?php

namespace TCS\CommandBundle\Job\Provider;

use Doctrine\ORM\EntityManagerInterface;
use TCS\CommandBundle\Entity\Job;

class ORMProvider implements ProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return \TCS\CommandBundle\Entity\Repository\JobRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository(Job::class);
    }

    /**
     * @inheritdoc
     */
    public function getScheduled()
    {
        return $this->getRepository()->findScheduled();
    }

    /**
     * @inheritdoc
     */
    public function find($id)
    {
        return $this->getRepository()->find($id);
    }
}