<?php

namespace TCS\CommandBundle\Job;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use TCS\CommandBundle\Command\JobableCommand;
use TCS\CommandBundle\Command\Locker;
use TCS\CommandBundle\Command\RegistryInterface;
use TCS\CommandBundle\Entity\Job;
use TCS\CommandBundle\Job\Context\KeyEncoder;

class Runner implements RunnerInterface
{

    /**
     * @var string
     */
    private $environment;

    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var Locker
     */
    private $locker;

    /**
     * @var OutputFactory
     */
    private $outputFactory;

    public function __construct($environment, RegistryInterface $registry, Locker $locker, OutputFactory $outputFactory)
    {
        $this->environment = $environment;
        $this->registry = $registry;
        $this->locker = $locker;
        $this->outputFactory = $outputFactory;
    }

    /**
     * @inheritdoc
     */
    public function run(Job $job)
    {
        if (!$job->isEnabled()) {
            throw new \RuntimeException('Job is not enabled');
        }

        $command = $this->registry->get($job->getCommandName());

        // check conflicts
        $conflicts = $command->getConflicts();
        foreach ($conflicts as $conflict) {
            $conflictingCommand = $this->registry->get($conflict);

            if ($this->locker->isLocked($conflictingCommand)) {
                throw new \RuntimeException('Job cannot run because of a currently running conflicting command : ' . $conflict);
            }
        }

        // check if command is actually running
        if ($this->locker->isLocked($command)) {
            throw new \RuntimeException('Command is currently running in another job');
        }

        $input = [
            '--env'            => $this->environment,
            '--no-debug'       => true,
            '--no-interaction' => true,
        ];

        $input = array_merge(
            $input,
            $job->getArguments(),
            $job->getOptions()
        );

        $input['command'] = $command->getName();

        $encoder = KeyEncoder::create();
        $contextKey = $encoder->createKey($job);
        $input['--' . JobableCommand::JOBCONTEXTKEY_PARAMETER] = $contextKey;

        $command->run(
            new ArrayInput($input),
            $this->outputFactory->create($encoder->decode($contextKey)[1])
        );
    }

}